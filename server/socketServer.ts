import { Server } from "http";
import { Server as ioServer, Socket } from "socket.io";

interface SocketMessage {
  (socket: Socket): void;
}

export class socketServer {
  private io: ioServer;

  constructor(server: Server) {
    this.io = new ioServer(server, {
      cors: {
        origin: "*",
        methods: ["GET", "POST"],
      },
    });
    return this;
  }

  listen(cb?: SocketMessage): void {
    this.io.on("connect", (socket: Socket) => {
      console.log("client connected", { id: socket.id });
      cb?.(socket);
      socket.on("disconnect", () =>
        console.log(`socket ${socket.id} disconnected`)
      );
    });
  }
}
