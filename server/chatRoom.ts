import { Socket } from "socket.io";
import { Room } from "./lib/livestream/room";
import {
    Worker,
    Router,
    WebRtcTransport,
    Producer,
    Consumer,
  } from "mediasoup/node/lib/types";

export class chatRoom extends Room {
    constructor(socket:Socket,worker:Worker,id:string) {
        super(socket,worker,id)
    }


    listen(socket:Socket):void{
        socket.on('pause-producer',({producerid},ackFn)=>{
            const producer = this.producers.find(item=>item.producer.id === producerid)
            producer?.producer.pause()
            socket.to(this.id).emit('producer-status' , {producerid , paused:true})
            ackFn({paused:true})

        })
        socket.on('resume-producer',({producerid},ackFn)=>{
            const producer = this.producers.find(item=>item.producer.id === producerid)
            producer?.producer.resume()
            socket.to(this.id).emit('producer-status' , {producerid , paused:false})

            ackFn({paused:false})
        })
    }

    async addPeer(socket:Socket){

        this.joinPeer(socket)
        this.listen(socket)
    }

}