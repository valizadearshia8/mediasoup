import express, { Application } from "express";
import { createServer, Server } from "http";
import { createServer as createSecureServer } from "https";
import { existsSync, readFileSync } from "fs";

import path from "path";
import cors from "cors";
import { config } from "./config";

const secureOptions =
  existsSync(config.https.tls.cert) && existsSync(config.https.tls.key)
    ? {
        cert: readFileSync(config.https.tls.cert),
        key: readFileSync(config.https.tls.key),
      }
    : false;

export class CoreServer {
  public readonly serverPort: number = config.https.listenPort;
  private app: Application = express();
  private server: Server =
  secureOptions
      ? createSecureServer(secureOptions,this.app)
      : createServer(this.app);

  constructor() {
    this.initialServer();
  }
  private initialServer(): void {
    this.passRoutes();
    this.server.listen(this.serverPort, () => {
      console.log(`http server up and running on port ${this.serverPort}`);
    });
  }
  private passRoutes(): void {
    this.app.get("/health", (req, res) => {
      res.send("up...!");
    });
    this.app.get("/", (req, res) => {
      res.send("welcome to service");
    });
    this.app.use(cors());
    this.app.use("/view", express.static(path.join(__dirname, "public")));
  }
  getApp(): Application {
    return this.app;
  }
  getServer(): Server {
    return this.server;
  }
}
