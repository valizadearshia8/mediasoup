import { config } from "../../config";
import * as mediasoup from "mediasoup";
import { Worker } from "mediasoup/node/lib/types";

export class WorkerService {
  public readonly workers: Worker[] = []
  private nextWorkerInd = 0;

  constructor() {
    this.initialWorker();
    return this;
  }

  async initialWorker(): Promise<void> {
    try {
      const worker = await mediasoup.createWorker({
        logLevel: config.mediasoup.workerSettings.logLevel,
        logTags: config.mediasoup.workerSettings.logTags,
        rtcMinPort: +config.mediasoup.workerSettings.rtcMinPort,
        rtcMaxPort: +config.mediasoup.workerSettings.rtcMaxPort,
      });
      console.log(worker);
      worker.on("died", (error) => {
        console.warn("worker died");
      });
      this.workers.push(worker);
    } catch (error) {
      throw error;
    }
  }

  getWorker():Worker {
    const worker = this.workers[this.nextWorkerInd];
    if (++this.nextWorkerInd === this.workers.length)
      this.nextWorkerInd = 0;
    return worker;
  }
}
