import { Socket } from "socket.io";
import {
  Worker,
  Router,
  WebRtcTransport,
  Producer,
  Consumer,
} from "mediasoup/node/lib/types";
import { config } from "../../config";

interface Peers {
  transports: string[];
  consumers: string[];
  producers: string[];
}

export class Room {
   worker: Worker;
   id: string;
  public readonly peers = new Map<string, Peers>();
  public router!: Router;
   socket: Socket;
   transports: Array<{
    socketid: string;
    transport: WebRtcTransport;
    consumer: boolean;
  }> = [];
   producers: Array<{
    socketid: string;
    producer: Producer;
  }> = [];
   consumers: Array<{
    socketid: string;
    consumer: Consumer;
  }> = [];

  constructor(socket: Socket, worker: Worker, id: string) {
    this.worker = worker;
    this.id = id;
    this.socket = socket;
    // this part is for the first person that creates the room
    this.initialRoom(socket);
    console.log(`new room created with id ${this.id}`);
  }

  async initRouter(): Promise<Router> {
    return await this.worker.createRouter({
      mediaCodecs: config.mediasoup.routerOptions.mediaCodecs,
    });
  }
  async initialRoom(socket: Socket): Promise<void> {
    const mediaCodecs = config.mediasoup.routerOptions.mediaCodecs;
    this.router = await this.worker.createRouter({ mediaCodecs });
    // this.joinPeer(socket);
    console.log(`router created with id ${this.router.id}`);
  }

  async joinPeer(socket: Socket) {
    if (this.peers.has(socket.id)) return;
    socket.join(this.id);
    this.peers.set(socket.id, {
      transports: [],
      producers: [],
      consumers: [],
    });
    this.socketListen(socket);
    socket.on("disconnect", () => this.removePeer(socket.id));

    console.log(`peer id ${socket.id} joined group ${this.id}`);
  }

  private socketListen(socket: Socket) {
    // all our room events will be handled in here

    socket.on("send-txt", (data) => {
      this.sendMessage(data.text, socket);
    });

    socket.on("get-rtpcapabilities", (socket, ackFn) => {
      ackFn(this.getRouterRtpCapabilities());
    });

    socket.on("create-transport", async ({ consumer }, ackFn) => {
      const transport = await this.createTransport();

      ackFn({
        params: {
          id: transport.id,
          iceParameters: transport.iceParameters,
          iceCandidates: transport.iceCandidates,
          dtlsParameters: transport.dtlsParameters,
        },
      });
      this.addToTransports(transport, consumer, socket.id);
    });

    socket.on("transport-connect", ({ dtlsParameters }) => {
      console.log("DTLS PARAMS... ", { dtlsParameters });

      this.getTransport(socket.id).connect({ dtlsParameters });
    });

    socket.on(
      "transport-produce",
      async ({ kind, rtpParameters, appData }, callback) => {
        // call produce based on the prameters from the client
        const producer = await this.getTransport(socket.id).produce({
          kind,
          rtpParameters,
        });

        this.addProducer(socket.id, producer);

        // announce the new producer to all peers
        socket.to(this.id).emit("new-producer", { producer: producer.id });

        console.log("Producer ID: ", producer.id, producer.kind);

        producer.on("transportclose", () => {
          console.log("transport for this producer closed ");
          producer.close();
        });

        // Send back to the client the Producer's id
        callback({
          id: producer.id,
          producersExist: this.producers.length > 1 ? true : false,
        });
      }
    );

    socket.on("get-producers", (ackFn) => {
      //  sends list of producers in the room
      var producersIds: string[] = [];
      this.producers.forEach((item) => {
        if (item.socketid !== socket.id) producersIds.push(item.producer.id);
      });
      ackFn(producersIds);
    });

    socket.on(
      "transport-recv-connect",
      async ({ dtlsParameters, serverConsumerTransportId }) => {
        console.log(`DTLS PARAMS: ${dtlsParameters}`);
        const consumerTransport = this.transports.find(
          (item) =>
            item.consumer && item.transport.id == serverConsumerTransportId
        )?.transport;
        await consumerTransport?.connect({ dtlsParameters });
      }
    );

    socket.on(
      "consume",
      async (
        { rtpCapabilities, remoteProducerId, serverConsumerTransportId },
        callback
      ) => {
        try {
          let consumerTransport = this.transports.find(
            (item) =>
              item.consumer && item.transport.id == serverConsumerTransportId
          )?.transport;

          // check if the router can consume the specified producer
          if (
            this.router.canConsume({
              producerId: remoteProducerId,
              rtpCapabilities,
            }) &&
            consumerTransport instanceof WebRtcTransport
          ) {
            // transport can now consume and return a consumer
            const consumer = await consumerTransport.consume({
              producerId: remoteProducerId,
              rtpCapabilities,
              paused: true,
            });

            consumer.on("transportclose", () => {
              console.log("transport close from consumer");
            });

            consumer.on("producerclose", () => {
              console.log("producer of consumer closed");
              socket.emit("producer-closed", { remoteProducerId });

              consumer.close();
              this.removeTransport(
                consumerTransport ? consumerTransport.id : "",
                consumer.id
              );
            });

            this.addConsumer(socket.id, consumer);

            // from the consumer extract the following params
            // to send back to the Client
            const params = {
              id: consumer.id,
              producerId: remoteProducerId,
              kind: consumer.kind,
              rtpParameters: consumer.rtpParameters,
              serverConsumerId: consumer.id,
            };

            // send the parameters to the client
            callback({ params });
          }
        } catch (error) {
          console.log(error);
          callback({
            params: {
              error: error,
            },
          });
        }
      }
    );

    socket.on("consumer-resume", ({ serverConsumerId }) => {
      const consumer = this.consumers.find(
        (item) => item.consumer.id === serverConsumerId
      );
      consumer?.consumer?.resume();
    });

    socket.emit("joined", { roomid: this.id });
  }

  sendMessage(msg: string, socket: Socket) {
    console.log("msg %d", msg);

    socket.to(this.id).emit("message", { msg });
  }

  getRouterRtpCapabilities() {
    return this.router.rtpCapabilities;
  }

   async createTransport(): Promise<WebRtcTransport> {
    let transport = await this.router.createWebRtcTransport(
      config.mediasoup.webRtcTransportOptions
    );
    transport.on("dtlsstatechange", (dtlsState) => {
      if (dtlsState === "closed") {
        transport.close?.();
      }
    });

    return transport;
  }

   addToTransports(
    transport: WebRtcTransport,
    consumer: boolean,
    socketid: string
  ) {
    this.transports.push({
      transport,
      consumer,
      socketid,
    });
    console.log(
      `new transport added direction: ${consumer ? "recv" : "send"} id: ${
        transport.id
      }`
    );

    this.peers.get(socketid)?.transports.push(transport.id);
  }

   removeTransport(
    transportid: string,
    consumerid?: string,
    producerid?: string
  ): void {
    this.transports = this.transports.filter(
      (transportData) => transportData.transport.id !== transportid
    );
    if (producerid)
      this.producers = this.producers.filter(
        (transportData) => transportData.producer.id !== producerid
      );
    if (consumerid)
      this.consumers = this.consumers.filter(
        (transportData) => transportData.consumer.id !== consumerid
      );
  }

   addProducer(socketid: string, producer: Producer) {
    this.producers.push({ socketid, producer });
    this.peers.get(socketid)?.producers.push(producer.id);
  }

   addConsumer(socketid: string, consumer: Consumer) {
    this.consumers.push({ socketid, consumer });
    this.peers.get(socketid)?.consumers.push(consumer.id);
  }

  getTransport(socketid: string) {
    const [producerTransport] = this.transports.filter(
      (transport) => transport.socketid === socketid && !transport.consumer
    );
    return producerTransport.transport;
  }

  removePeer(socketid: string) {
    this.consumers = this.consumers.filter((item) => {
      if (item.socketid === socketid) item.consumer.close();
      else return item;
    });

    this.producers = this.producers.filter((item) => {
      if (item.socketid === socketid) item.producer.close();
      else return item;
    });

    this.transports = this.transports.filter((item) => {
      if (item.socketid === socketid) item.transport.close();
      else return item;
    });

    this.peers.delete(socketid);
    if (this.peers.size === 0) this.closeRoom();

    console.log(`peer ${socketid} removed`);
    
  }

  closeRoom(): void {
    this.router.close();
    this.socket.to(this.id).disconnectSockets(true);
    this.socket.disconnect(true);
  }
}
