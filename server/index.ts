import { CoreServer } from "./server";
import { socketServer } from "./socketServer";
import { WorkerService } from "./lib/livestream/worker";
import { chatRoom } from "./chatRoom";

let ApplicationRooms = new Map<string, chatRoom>();

const run = async () => {
  const coreServer = new CoreServer();
  const workers = new WorkerService();
  const app = coreServer.getApp();
  const server = coreServer.getServer();
  const ioServer = new socketServer(server);

  ioServer.listen((socket) => {
    socket.on("test", () => {
      console.log("tested", socket.id);
    });

    socket.on("join-room", (data) => {
      const { roomid } = data;
      let _room = ApplicationRooms.get(roomid);

      // check for user permissions on this room

      if (!_room) {
        _room = new chatRoom(socket, workers.getWorker(), roomid);
        ApplicationRooms.set(roomid, _room);
      }
      _room?.addPeer(socket);
    });
  });

  app.use("/test", (req, res) => res.send("test"));
};

run();
