import "./App.css";
import { useEffect, useRef, useState } from "react";
import { io } from "socket.io-client";
import * as mediasoupClient from "mediasoup-client";
import VideoCard from "./components/videoCard";
import { BsMicMute, BsBroadcast } from "react-icons/bs";
import { BiLogOut } from "react-icons/bi";

let params = {
  // mediasoup params
  encodings: [
    {
      rid: "r0",
      maxBitrate: 100000,
      scalabilityMode: "S1T3",
    },
    {
      rid: "r1",
      maxBitrate: 300000,
      scalabilityMode: "S1T3",
    },
    {
      rid: "r2",
      maxBitrate: 900000,
      scalabilityMode: "S1T3",
    },
  ],
  // https://mediasoup.org/documentation/v3/mediasoup-client/api/#ProducerCodecOptions
  codecOptions: {
    videoGoogleStartBitrate: 1000,
  },
};

let rtpCapabilities;
let device;
let producerTransport;
let producer;
let consumerTransports = [];

const App = () => {
  const [status, setStatus] = useState({
    state: "no-conn",
    msg: "try joining a room",
  });
  const [isRec, recording] = useState(false);

  const [socket, setSocket] = useState();
  const [roomid, setRoom] = useState("");

  const videView = useRef();

  const [remotePeers, setRemotePeers] = useState([]);

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({ audio: true, video: true })
      .then((stream) => {
        videView.current.srcObject = stream;
        const track = stream.getVideoTracks()[0];
        params = { ...params, track };
      })
      .catch((err) => console.log(err));
    start();
  }, []);

  const start = () => {
    const socket = io(process.env.LIVE_SERVER,{
      // auth: {
      //   token: "missing"
      // }
    });
    setSocket(socket);
  };

  const connectToRoom = async () => {
    setStatus({
      state: "connecting",
      msg: `connecting to room ${roomid}`,
    });
    await socket.emit("join-room", { roomid });
    socket.on("new-producer", ({ producer }) => getConsumerTransport(producer));

    socket.on("joined", ({ roomid }) => {
      setStatus({ state: "conn-est", msg: `connected to room ${roomid}` });
      socket.emit("get-rtpcapabilities", {}, async (data) => {
        rtpCapabilities = data;
        await createDevice();
        createProduceTransport();
      });

      socket.on("producer-status", ({ producerid, paused }) =>
        setmute(producerid, paused)
      );
    });
  };

  const createDevice = async () => {
    device = new mediasoupClient.Device();
    await device.load({
      routerRtpCapabilities: rtpCapabilities,
    });
  };

  const createProduceTransport = () => {
    socket.emit("create-transport", { consumer: false }, async ({ params }) => {
      producerTransport = await device.createSendTransport(params);
      producerTransport.on(
        "connect",
        async ({ dtlsParameters }, callback, errback) => {
          try {
            await socket.emit("transport-connect", {
              dtlsParameters,
            });

            callback();
          } catch (error) {
            errback(error);
          }
        }
      );

      producerTransport.on("produce", async (parameters, callback, errback) => {
        console.log(parameters);

        try {
          // tell the server to create a Producer
          // with the following parameters and produce
          // and expect back a server side producer id
          // see server's socket.on('transport-produce', ...)
          await socket.emit(
            "transport-produce",
            {
              kind: parameters.kind,
              rtpParameters: parameters.rtpParameters,
              appData: parameters.appData,
            },
            ({ id, producersExist }) => {
              // Tell the transport that parameters were transmitted and provide it with the
              // server side producer's id.
              callback({ id });

              // if producers exist, then join room
              if (producersExist) getProducers();
            }
          );
        } catch (error) {
          errback(error);
        }
      });

      console.log(producerTransport);
      connectSendTransport();
    });
  };

  const connectSendTransport = async () => {
    producer = await producerTransport.produce(params);
    recording(true);

    producer.on("trackended", () => {
      console.log("track ended");

      // close video track
    });

    producer.on("transportclose", () => {
      console.log("transport ended");

      // close video track
    });
  };

  const getProducers = () => {
    socket.emit("get-producers", (producerIds) => {
      console.log(producerIds);
      producerIds.forEach(getConsumerTransport);
    });
  };

  const getConsumerTransport = async (producerid) => {
    socket.emit("create-transport", { consumer: true }, async ({ params }) => {
      let consumerTransport = await device.createRecvTransport(params);
      consumerTransport.on(
        "connect",
        async ({ dtlsParameters }, callback, errback) => {
          try {
            await socket.emit("transport-recv-connect", {
              dtlsParameters,
              serverConsumerTransportId: params.id,
            });

            callback();
          } catch (error) {
            errback(error);
          }
        }
      );
      consumerTransport.on("close", () => console.log("closed"));

      console.log(consumerTransport);
      connectRecvTransport(consumerTransport, producerid, params.id);
    });
  };

  const connectRecvTransport = async (
    consumerTransport,
    remoteProducerId,
    serverConsumerTransportId
  ) => {
    await socket.emit(
      "consume",
      {
        rtpCapabilities: device.rtpCapabilities,
        remoteProducerId,
        serverConsumerTransportId,
      },
      async ({ params }) => {
        if (params.error) {
          console.log("Cannot Consume");
          return;
        }

        console.log(`Consumer params`, params);
        const consumer = await consumerTransport.consume({
          id: params.id,
          producerId: params.producerId,
          kind: params.kind,
          rtpParameters: params.rtpParameters,
        });

        consumerTransports.push({
          consumerTransport,
          serverConsumerTransportId: params.id,
          producerId: remoteProducerId,
          consumer,
        });
        const { track } = consumer;

        let newvid = remotePeers;
        newvid.push({ producerid: remoteProducerId, track, paused: false });
        setRemotePeers([...newvid]);
        console.log(remotePeers.length);

        socket.emit("consumer-resume", {
          serverConsumerId: params.serverConsumerId,
        });
      }
    );
  };

  const muteProduce = (producerid) => {
    remotePeers.map((item) => {
      if (item.producerid !== producerid) return;
      item.paused
        ? socket.emit("resume-producer", { producerid }, ({ paused }) =>
            setmute(producerid, paused)
          )
        : socket.emit("pause-producer", { producerid }, ({ paused }) =>
            setmute(producerid, paused)
          );
    });
  };
  const setmute = (producerid, paused) => {
    const newPeers = remotePeers.map((item) => {
      if (item.producerid === producerid) item.paused = paused;
      return item;
    });
    setRemotePeers([...newPeers]);
  };

  return (
    <div className="App">
      {status.state === "no-conn" ? (
        <div className="input-box">
          <input
            type="text"
            name="roomid"
            onChange={(val) => setRoom(val.target.value)}
            value={roomid}
          />
          <button onClick={connectToRoom}>Connect</button>
        </div>
      ) : status.state === "connecting" ? (
        <div>
          <h4>{status.msg}</h4>
        </div>
      ) : status.state === "conn-est" ? (
        <div>
          <h4>{status.msg}</h4>
        </div>
      ) : (
        <div>
          <h4>{status.msg}</h4>
        </div>
      )}

      <div
        className={`myCam ${
          status.state === "connecting" ? "connecting" : isRec && "rec"
        }`}
      >
        {status.state === "conn-est" && (
          <ul>
            <li>
              <button>
                <BsMicMute />
              </button>
            </li>
            <li>
              <button>
                <BsBroadcast />
              </button>
            </li>
            <li>
              <button>
                <BiLogOut />
              </button>
            </li>
          </ul>
        )}

        <video ref={videView} autoPlay />
      </div>
      {status.state === "conn-est" && (
        <div className="counter">
          {" "}
          <h4>{`${remotePeers.length} clients in the room`}</h4>
        </div>
      )}

      <div className="remote-box">
        {remotePeers.map((item, key) => {
          return (
            <VideoCard
              key={key}
              track={item.track}
              onMute={() => muteProduce(item.producerid)}
              isMute={item.paused}
            />
          );
        })}
      </div>
    </div>
  );
};

export default App;
