import { useEffect, useRef } from "react";
import "./videoCard.css";
import { BsMicMute } from "react-icons/bs";

const VideoCard = ({ track, onMute ,isMute}) => {
  const stream = new MediaStream([track]);
  const remoteVideo = useRef();

  console.log(remoteVideo);
  // useEffect(()=>{
  //     remoteVideo.current.srcObject = new MediaStream([
  //         track,
  //       ]);
  // },[track])

  useEffect(() => {
    console.log(remoteVideo);
    remoteVideo.current.srcObject = stream;
  });

  return (
    <div className="videoCard">
      <video ref={remoteVideo} autoPlay />
      <button onClick={onMute} className={isMute && 'mute'}>
        <BsMicMute />
      </button>
    </div>
  );
};

export default VideoCard;
